package main

import (
	"fmt"
	"regexp"
	"os"
	"bufio"
	"strings"
)


func Readln(r *bufio.Reader) (string, error) {
  var (isPrefix bool = true
       err error = nil
       line, ln []byte
      )
  for isPrefix && err == nil {
      line, isPrefix, err = r.ReadLine()
      ln = append(ln, line...)
  }
  return string(ln),err
}


func stringInSlice(a string, list []string) []string {
    for _, b := range list {
        if strings.Split(b,"(")[0] == strings.Split(a,"(")[0] || strings.Split(a,"(")[0]=="" {
            return list
        }
    }
    return append(list,a)
}

func main() {
	file, err3 := os.Open(os.Args[1] )
    if err3!=nil {
    	panic(err3)
    }

    var mode string
    var seahorns map[string][]string
    var next_threads []string
    seahorns=make(map[string][]string)
	
	inv_str := "([a-zA-Z_][a-zA-Z_0-9]*)[(](([a-zA-Z_0-9\\+\\-]*,)*)([a-zA-Z_0-9\\+\\-]+)[)]"
	inv := regexp.MustCompile(inv_str)
	normal_str:="(" + inv_str + ")" + "[ ]*:-[ ]*" + "(" + inv_str + ")"
	normal:=regexp.MustCompile(normal_str+"\\.")
	with_ors_str:=normal_str+"( ; "+inv_str+")*"
	with_ors:=regexp.MustCompile(with_ors_str+"\\.")
	with_ands_str:=normal_str+"(,"+inv_str+")*"
	with_ands:=regexp.MustCompile(with_ands_str+"\\.")
	with_conditions_and_ands_str:="(" + inv_str + ")" + "[ ]*:-[ ]*" + "(" + inv_str + ")*"+"(,"+inv_str+")*.*"
	with_conditions_and_ands:=regexp.MustCompile(with_conditions_and_ands_str)
		

	r := bufio.NewReader(file)
	s, e := Readln(r)
	for ;e == nil;s,e = Readln(r) { 
		if s=="" {
		   	continue
		}   
		if strings.Contains(s, "Variable List") {
			mode="Variable List"
		}else if strings.Contains(s, "Transitions") {
			mode="Relations"			
		}else if strings.Contains(s, "Init") {
			mode="Relations"			
		}else if strings.Contains(s, "Rules") {
			mode="Rules"			
		}else if strings.Contains(s, "Scope Symbols") {
			break
		}else{
			if mode=="Variable List" {
				seahorns[mode]=strings.Split(s[2:len(s)-3]," ")
			}else if mode=="Relations" {
				if strings.Contains(s,"init") {
					s=s[2:len(s)-2]
					continue
				}
				if s!="" {seahorns["Relations"]=stringInSlice(strings.Split(s," :- ")[0]+" ",seahorns["Relations"])}
				if s!="" {
					s=s[0:len(s)-1]
					splits:=strings.Split(s," :- ")
					rule:=strings.Replace(inv.ReplaceAllString(splits[0],"($1 "+ "$2"  + "$4"),","," ",-1)
					splits_args:=strings.Split(rule," ")[1:]
					for i,_ := range splits_args{
						cond := regexp.MustCompile("(.*)\\+(.*)")
						splits_args[i]=cond.ReplaceAllString(splits_args[i],"(+ $1 $2)")
						cond = regexp.MustCompile("(.*)\\-(.*)")
						splits_args[i]=cond.ReplaceAllString(splits_args[i],"(- $1 $2)")
						
					}
					index :=len(splits_args) - len(seahorns["Variable List"]) +1 
					splits_args2:=make([]string, len(splits_args)) 
					copy(splits_args2,splits_args)
					splits_args2=splits_args2[0:index]
					for ij := 1; ij < len(seahorns["Variable List"]); ij++ {
						splits_args2=append(splits_args2,seahorns["Variable List"][ij] + "1")
					}
					//fmt.Println(splits_args2,index,len(splits_args))
					rule2:="(and (not (and "
					for ij := index; ij < len(splits_args); ij++ {
						rule2=rule2+"( = " + splits_args[ij] + " " + splits_args2[ij]  + " )"
					}

					rule=strings.Split(rule," ")[0]+" "+ strings.Join(splits_args," ") +")"
					rule2=rule2 + ") )" + strings.Split(rule," ")[0]+" "+ strings.Join(splits_args2," ") +")" + ")"
					
					if len(splits)==1 {
						next_threads=append(next_threads,"(rule " + rule + ")")
						//next_threads=append(next_threads,"(rule( => " + rule2 + " Post_Condition ))")
					}else{
						//next_threads=append(next_threads,"(rule( => true " + rule + "))")
						//next_threads=append(next_threads,"(rule " + rule + ")")
						//next_threads=append(next_threads,"(rule( => " + rule2 + " Post_Condition ))")
						seahorns["Rules"]=append(seahorns["Rules"],s+".")
					}
				}
			}else if mode=="Rules" {
				if s!="" {seahorns["Rules"]=append(seahorns["Rules"],s)}
				if s!="" {seahorns["Relations"]=stringInSlice(strings.Split(s,":-")[0],seahorns["Relations"])}
			}
		}
		
	}
	for i := 1; i < len(seahorns["Variable List"]); i++ {
		fmt.Println("(declare-var " + seahorns["Variable List"][i] + " Int )")
		fmt.Println("(declare-var " + seahorns["Variable List"][i] + "1 Int )")
		fmt.Println("(declare-var " + seahorns["Variable List"][i] + "_ Int )")
	}
	for i := 0; i < len(seahorns["Relations"]); i++ {
		rel_name:=strings.Split(seahorns["Relations"][i],"(")[0]
		if len(rel_name)<3 || (rel_name[0:3]!="inv"&&rel_name[0:3]!="nex"&&rel_name[0:3]!="dne"&&rel_name[0:3]!="ini") {
			continue
		}
		Variable_Count := len(strings.Split(seahorns["Relations"][i],","))
		args:=""
		for j := 0; j < Variable_Count; j++ {
			args=args+" Int" 
		}
		fmt.Println("(declare-rel " + rel_name + "(" + args + " ))")
/*		if rel_name[0:3]=="inv" {
			fmt.Println("(rule( " + rel_name + " " + strings.Join(seahorns["Variable List"][1:]," ") + " ))")
		}*/
	}
	fmt.Println("(declare-rel " + "Post_Condition " + "(" + "))")
	for i := 0; i < len(seahorns["Rules"]); i++ {
		s=seahorns["Rules"][i]
			//fmt.Println(s)
		if normal.MatchString(s) {
			s=s[0:len(s)-1]
			splits:=strings.Split(s," :- ")
			fmt.Println("(rule(=>"+strings.Replace(inv.ReplaceAllString(splits[1],"($1 $2 " + "$4"),","," ",-1)+")" + strings.Replace(inv.ReplaceAllString(splits[0],"($1 $2 " + "$4"),","," ",-1)+") "  +" ) )")
		}else if with_ors.MatchString(s) {
			s=s[0:len(s)-1]
			splits:=strings.Split(s," :- ")
			ii:=splits[0]
			str:="(rule(=>" 
			str=str+"(or "
			splits=strings.Split(splits[1],";")
			for _,j := range splits{
				str=str+strings.Replace(inv.ReplaceAllString(j,"($1 "+ "$2"  + " $4"),","," ",-1)+") "
			}
			str=str+") "+strings.Replace(inv.ReplaceAllString(ii,"($1 "+ "$2"  + " $4"),","," ",-1)+") "+") )"
			fmt.Println(str)
		}else if with_ands.MatchString(s) {
			s=s[0:len(s)-1]
			splits:=strings.Split(s," :- ")
			ii:=splits[0]
			str:="(rule(=>"
			str=str+"(and "
			splits=strings.Split(splits[1],"),")
			inv := regexp.MustCompile(inv_str[0:len(inv_str)-3])
			for _,j := range splits{
				str=str+strings.Replace(inv.ReplaceAllString(j,"($1 "+ "$2"  + " $4"),","," ",-1)+") "
			}
			str=str+strings.Replace(inv.ReplaceAllString(ii,"($1 "+ "$2"  + " $4"),","," ",-1)+") " +")"
			fmt.Println(str)
		}else if with_conditions_and_ands.MatchString(s) {
			s=s[0:len(s)-1]
			splits:=strings.Split(s," :- ")
			ii:=splits[0]
			str:="(rule(=>"
			ij:=len(str)
			//str=str+"(and "
			splits=strings.Split(splits[1],"),")
			inv := regexp.MustCompile(inv_str[0:len(inv_str)-3])
			j:=1
			for ; j < len(splits); j++ {
				str=str+strings.Replace(inv.ReplaceAllString(splits[j-1],"($1 "+ "$2"  + " $4"),","," ",-1)+") "
			}
			j=j-1
			cond := regexp.MustCompile("([a-zA-Z_0-9]+)=<([a-zA-Z_0-9]+)")
			splits[j]=cond.ReplaceAllString(splits[j],"  ( <= $1 $2) ")
			cond = regexp.MustCompile("([a-zA-Z_0-9]+)>=([a-zA-Z_0-9]+)")
			splits[j]=cond.ReplaceAllString(splits[j],"  ( >= $1 $2) ")
			cond = regexp.MustCompile("([a-zA-Z_0-9]+)=\\\\=([a-zA-Z_0-9]+)")
			splits[j]=cond.ReplaceAllString(splits[j]," ( not  ( = $1 $2 ))")
			cond = regexp.MustCompile("([a-zA-Z_0-9]+)=([a-zA-Z_0-9]+)")
			splits[j]=cond.ReplaceAllString(splits[j]," ( = $1 $2 )")
			cond = regexp.MustCompile("([a-zA-Z_0-9]+)<([a-zA-Z_0-9]+)")
			splits[j]=cond.ReplaceAllString(splits[j],"  ( < $1 $2) ")
			cond = regexp.MustCompile("([a-zA-Z_0-9]+)>([a-zA-Z_0-9]+)")
			splits[j]=cond.ReplaceAllString(splits[j],"  ( > $1 $2) ")

			and_str := regexp.MustCompile("([^,]+),([^,]+)(.*)")
			or_str := regexp.MustCompile("([^;]+);([^;]+)(.*)")

			and_str2 := regexp.MustCompile("[(]and ([^,]+)[)][ ]*,([^,]+)(.*)")
			or_str2 := regexp.MustCompile("[(]or ([^;]+)[)][ ]*;([^;]+)(.*)")
			for ; ;  {
				tmp:=splits[j]
				splits[j]=and_str.ReplaceAllString(splits[j],"(and $1 $2) $3")
				splits[j]=or_str.ReplaceAllString(splits[j],"(or $1 $2) $3")
				
				for ; ;  {
					tmp:=splits[j]
					splits[j]=and_str2.ReplaceAllString(splits[j],"(and $1 $2) $3")
					splits[j]=or_str2.ReplaceAllString(splits[j],"(or $1 $2) $3")
					if tmp==splits[j] {
						break
					}

				}
				if tmp==splits[j] {
					break
				}
			}
			str=str + splits[j]
			str2:=str[ij:len(str)]
			str=str+strings.Replace(inv.ReplaceAllString(ii,"($1 "+ "$2"  + " $4"),","," ",-1)+") " +" ) "
			fmt.Println(str)
			str2 = "(rule(=> ( and ( not " + str2 + "))" + strings.Replace(inv.ReplaceAllString(ii,"($1 "+ "$2"  + " $4"),","," ",-1) + ") Post_Condition " +") " +" ) "
			/*if strings.Contains(str2,"init") {
				fmt.Println(str2)
			}*/
		}else {
			//post condition
			s=s[0:len(s)-1]
			splits:=strings.Split(s,":-")
			//fmt.Println(splits)
			str:="(rule(=>( and "
			j:=0
			cond := regexp.MustCompile("([a-zA-Z_0-9]+)=<([a-zA-Z_0-9]+)")
			splits[j]=cond.ReplaceAllString(splits[j],"  ( <= $1 $2) ")
			cond = regexp.MustCompile("([a-zA-Z_0-9]+)>=([a-zA-Z_0-9]+)")
			splits[j]=cond.ReplaceAllString(splits[j],"  ( >= $1 $2) ")
			cond = regexp.MustCompile("([a-zA-Z_0-9]+)=\\\\=([a-zA-Z_0-9]+)")
			splits[j]=cond.ReplaceAllString(splits[j]," ( not  ( = $1 $2 ))")
			cond = regexp.MustCompile("([a-zA-Z_0-9]+)=([a-zA-Z_0-9]+)")
			splits[j]=cond.ReplaceAllString(splits[j]," ( = $1 $2 )")
			cond = regexp.MustCompile("([a-zA-Z_0-9]+)<([a-zA-Z_0-9]+)")
			splits[j]=cond.ReplaceAllString(splits[j],"  ( < $1 $2) ")
			cond = regexp.MustCompile("([a-zA-Z_0-9]+)>([a-zA-Z_0-9]+)")
			splits[j]=cond.ReplaceAllString(splits[j],"  ( > $1 $2) ")

			and_str := regexp.MustCompile("([^,]+),([^,]+)(.*)")
			or_str := regexp.MustCompile("([^;]+);([^;]+)(.*)")

			and_str2 := regexp.MustCompile("[(]and ([^,]+)[)][ ]*,([^,]+)(.*)")
			or_str2 := regexp.MustCompile("[(]or ([^;]+)[)][ ]*;([^;]+)(.*)")
			for ; ;  {
				tmp:=splits[j]
				splits[j]=and_str.ReplaceAllString(splits[j],"(and $1 $2) $3")
				splits[j]=or_str.ReplaceAllString(splits[j],"(or $1 $2) $3")
				
				for ; ;  {
					tmp:=splits[j]
					splits[j]=and_str2.ReplaceAllString(splits[j],"(and $1 $2) $3")
					splits[j]=or_str2.ReplaceAllString(splits[j],"(or $1 $2) $3")
					if tmp==splits[j] {
						break
					}

				}
				if tmp==splits[j] {
					break
				}
			}
			final:=splits[j]
			str=str 
			str=str+"(and "
			splits=strings.Split(splits[1],"),")
			for ; ;  {
				tmp:=splits[j]
				splits[j]=and_str.ReplaceAllString(splits[j],"(and $1 $2) $3")
				splits[j]=or_str.ReplaceAllString(splits[j],"(or $1 $2) $3")
				
				for ; ;  {
					tmp:=splits[j]
					splits[j]=and_str2.ReplaceAllString(splits[j],"(and $1 $2) $3")
					splits[j]=or_str2.ReplaceAllString(splits[j],"(or $1 $2) $3")
					if tmp==splits[j] {
						break
					}
				}
				cond := regexp.MustCompile("([a-zA-Z_0-9]+)=<([a-zA-Z_0-9]+)")
				splits[j]=cond.ReplaceAllString(splits[j],"  ( <= $1 $2) ")
				cond = regexp.MustCompile("([a-zA-Z_0-9]+)>=([a-zA-Z_0-9]+)")
				splits[j]=cond.ReplaceAllString(splits[j],"  ( >= $1 $2) ")
				cond = regexp.MustCompile("([a-zA-Z_0-9]+)=\\\\=([a-zA-Z_0-9]+)")
				splits[j]=cond.ReplaceAllString(splits[j]," ( not  ( = $1 $2 ))")
				cond = regexp.MustCompile("([a-zA-Z_0-9]+)=([a-zA-Z_0-9]+)")
				splits[j]=cond.ReplaceAllString(splits[j]," ( = $1 $2 )")
				cond = regexp.MustCompile("([a-zA-Z_0-9]+)<([a-zA-Z_0-9]+)")
				splits[j]=cond.ReplaceAllString(splits[j],"  ( < $1 $2) ")
				cond = regexp.MustCompile("([a-zA-Z_0-9]+)>([a-zA-Z_0-9]+)")
				splits[j]=cond.ReplaceAllString(splits[j],"  ( > $1 $2) ")
				if tmp==splits[j] {
					break
				}
			}
			inv := regexp.MustCompile(inv_str[0:len(inv_str)-3])
			for _,j := range splits{
				str=str+strings.Replace(inv.ReplaceAllString(j,"($1 "+ "$2"  + " $4"),","," ",-1)+") "
			}
			str=str+ "( not" +  final + ")" +" ) Post_Condition ) ) "
			fmt.Println(str)
			//str="(rule(=>  "+final+" Post_Condition  ))"
			//fmt.Println(str)
		}
	}
	for i := 0; i < len(next_threads); i++ {
		fmt.Println(next_threads[i])
	}
		fmt.Println("(query Post_Condition)")
}