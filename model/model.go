package model

import "regexp"
import "bufio"
import "strings"
import "os"

type Line struct{
	Dest string // x fro x=a
	Src string // a fro x=a
	Flush bool // is it update atomic
	Ite_Flush bool // ite atomic?
	Ite bool // ite?
	Extra_Pre []string // case splitting oer some condition, stronger assignment rule in paper
	Hint []string // hints
	Ite_statement []string // statements in ite
}

type Thread struct{
	Lines []Line
	Local_Variables_Str []string
}

type Base_Code struct {
	Pre_Condition string
	Threads []Thread
	Post_Condition string
}

type Final_Code struct{
	Init []string
	Transitions []string
	Locals []string
	Summary []string
	Stability []string
	Post_Condition []string
	Scope_Symbols []string
}

var Splitter_B = regexp.MustCompile("::=")
var Splitter_A = regexp.MustCompile("[a-zA-Z_][a-zA-Z_0-9]*")
var Splitter_C = regexp.MustCompile("^[0-9]+$")

type Stack struct {
	top *Element
	size int
}

type Element struct {
	value interface{}
	next *Element
}


func (s *Stack) Len() int {
	return s.size
}

func (s *Stack) Push(value interface{}) {
	s.top = &Element{value, s.top}
	s.size++
}


func (s *Stack) Pop() (value interface{}) {
	if s.size > 0 {
		value, s.top = s.top.value, s.top.next
		s.size--
		return
	}
	return nil
}

func Print_array(array []string,file *os.File) {
	for _,element := range array{
		 _, _ = file.WriteString(element)
	_,_ = file.WriteString("\n")
	}
	_,_ = file.WriteString("\n")
}

func Readln(r *bufio.Reader) (string, error) {
  var (isPrefix bool = true
       err error = nil
       line, ln []byte
      )
  for isPrefix && err == nil {
      line, isPrefix, err = r.ReadLine()
      ln = append(ln, line...)
  }
  return string(ln),err
}

func IndexInSlice(a string, list []string) int {
    for i, b := range list {
        if b == a {
            return i
        }
    }
    return -1
}

//oldnew .. string. performed in order without overlapping
var Negators = strings.NewReplacer( "=\\=","=", "=", "=\\=",",", ";",";", ",","=<" , ">",	">=" , "<",	">" , "=<",	"<" , ">=")
