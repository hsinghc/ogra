This project is an implementation of the paper Owicki-Gries Reasoning for Weak Memory Models by Ori Lahav and Viktor Vafeiadis. This basically translates a pseudocode(see Documentation.pdf) into horn clauses according the OGRA logic described in the paper. 



## System Requirements

1. Compiler for golang

	`sudo apt-get install golang` or  https://golang.org/doc/install

2. linux based OS

3. Horn-clause solver - currently only qarmc and spacer are supported

	getting qarmc - https://www7.in.tum.de/~popeea/research/synthesis/

	getting spacer - https://bitbucket.org/spacer/code/src

---

## Usage Instructions

### With qarmc
Simply run the file main.go as

	`go run controller/main.go $pseudocode $tool(optional) --ns(optional)`

	--ns is optional no simplify flag which turns off the automatic smplification of the result. Horn
	clauses will be generated in file pseudocode.in and proof in pseudocode.out

Example Usage - 

	`go run main.go test_cases/t5 ./qarmc` 



This will generate test_cases/t5.out with the proof of the program if found and test_cases/t5.in as the input horn-clauses. To generate just horn-clauses, just skip the $tool.

### With spacer
First generate the horn -clauses and then translate them to smt2 format required for spacer.

Example Usage - 

	`go run main.go test_cases/t5`
	`go run tosmt2.go test_cases/t5.in > sp_test_cases/sp_t5`

then solve it using he spacer script solve.py available with this repository. move it to build folder in the spacer repository i.e. code/build/

then solve it as - 

	`python solve.py <path to sp_t5> > sp_t5.out`

	sp_t5.out is the required proof.

For any bug/suggestions, please send an email at hsingh150109@gmail.com or pull requests are more than welcome.
