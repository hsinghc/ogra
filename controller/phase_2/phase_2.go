package phase_2

import (
	"../../model"
	"strconv"
	"strings"
	//"fmt"
)

func Parse_To_Transition_Code(symbols []string,all_symbols []string,code *model.Base_Code,Final_Code *model.Final_Code) {

	var str string
	var str_divider string

	pre:=code.Pre_Condition
	var global_var_str string
	var var_str string
	var var_str_ string
	var var_str_prime string
	var transitions_str string
	var transitions_str_big string

	for _,variable := range symbols{
				global_var_str=global_var_str+variable+","
			}

	for _,variable := range all_symbols{
				var_str=var_str+variable+","
				var_str_=var_str_+variable+"_,"
				var_str_prime=var_str_prime+variable+"_prime,"
			}
	global_var_str=global_var_str[0:len(global_var_str)-1]
	var_str=var_str[0:len(var_str)-1]
	var_str_=var_str_[0:len(var_str_)-1]
	var_str_prime=var_str_prime[0:len(var_str_prime)-1]


	str="init(" + var_str + ") :- " + pre + " ."
	Final_Code.Init=append(Final_Code.Init,str + "\n")

/*	str=pre + ":-" + "init(" + global_var_str + ")."
	Final_Code.Init=append(Final_Code.Init,str + "\n")*/

	Final_Code.Post_Condition=append(Final_Code.Post_Condition,code.Post_Condition )

	str = "scope_symbols(" + "[" + strings.ToLower(global_var_str) + "]," + " [init]" + ")."
	Final_Code.Scope_Symbols=append(Final_Code.Scope_Symbols,str)

	for indt, thread := range code.Threads{
		for indl := 0; indl < len(thread.Lines); indl++ {
			line:=thread.Lines[indl]
			dst_holder:=line.Dest
			src_holder:=line.Src
			if model.Splitter_C.MatchString(line.Dest) {
				i, _ := strconv.Atoi(line.Dest)
				str="next_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(i-1) + "("
				str_divider="dnext_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(i-1) + "("
				transitions_str=transitions_str+"next_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(i-1) + ","
				transitions_str_big=transitions_str_big+"dnext_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(i-1) + ","
					if thread.Lines[indl].Ite{
						line.Dest=line.Ite_statement[0]+"::="+line.Ite_statement[1]
						line.Src=""
					}
			}else{
				str="next_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(indl+1) + "("
				str_divider="dnext_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(indl+1) + "("
				transitions_str=transitions_str+"next_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(indl+1) + ","
				transitions_str_big=transitions_str_big+"dnext_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(indl+1) + ","
			}
			str=str + var_str  +  ",";
			str_divider=str_divider + var_str_ + "," + var_str + ",";

			for _,variable := range all_symbols{
				if variable=="" {
					continue
				}
				var Src_Array []string
				Dst_Array:=strings.Split((model.Splitter_B.Split(line.Dest, -1))[0],",")
				Index := model.IndexInSlice(variable,Dst_Array)
				if Index!=-1 {
					if line.Src=="" { // atomic ones
						//fmt.Println(line.Dest)
						splits:=strings.Split(line.Dest,"::=")
						line.Dest=splits[0]
						line.Src=splits[1]
						Src_Array=strings.Split(line.Src,",")
						thread.Lines[indl].Dest=splits[0]
						thread.Lines[indl].Src=splits[1]
						//line.Src= line.Dest[0:len(line.Dest)-1] + "1"
						thread.Lines[indl].Flush=true;
						str_divider=str_divider+Src_Array[Index]+","				
					}else{
						Src_Array=strings.Split(line.Src,",")
						if thread.Lines[indl].Flush{
							str_divider=str_divider+Src_Array[Index]+","
						}else{
							str_divider=str_divider+model.Splitter_A.ReplaceAllString(Src_Array[Index], "${0}_")+","
						}
					}			
					str=str+Src_Array[Index] + ","	
				}else{
					str=str+variable+","
					str_divider=str_divider+variable+","
				}
			}
			line.Dest=dst_holder
			thread.Lines[indl].Dest=dst_holder
			line.Src=src_holder
			thread.Lines[indl].Src=src_holder
			str=str[0:len(str)-1]+")."
			str_divider=str_divider[0:len(str_divider)-1]+")."
			if model.Splitter_C.MatchString(thread.Lines[indl].Dest) {
				line=thread.Lines[indl];
				if line.Src!=""{str=str[0:len(str)-1]+ " :- " + line.Src +"."}
				if line.Src!=""{str_divider=str_divider[0:len(str_divider)-1]+ " :- " + model.Splitter_A.ReplaceAllString(line.Src, "${0}_") +"."}

			}
			Final_Code.Transitions=append(Final_Code.Transitions,str)
			Final_Code.Transitions=append(Final_Code.Transitions,str_divider)
			if  model.Splitter_C.MatchString(thread.Lines[indl].Dest) && line.Src!=""  {
				str="next_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(indl+1) + "("
				str_divider="dnext_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(indl+1) + "("

				if !thread.Lines[indl].Ite{
					transitions_str=transitions_str+"next_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(indl+1) + ","
					transitions_str_big=transitions_str_big+"dnext_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(indl+1) + ","
				}
				changed_var_str:=var_str
				if thread.Lines[indl].Ite{
					thread.Lines[indl].Flush=thread.Lines[indl].Ite_Flush
					thread.Lines[indl].Dest="blah"
					changed_var_str=strings.Replace(var_str,thread.Lines[indl].Ite_statement[0]+",",thread.Lines[indl].Ite_statement[2]+",",-1)
					thread.Lines[indl].Ite_statement=[]string{}
				}
				str=str + var_str + ","+ changed_var_str + ",";
				str_divider=str_divider + var_str_ + "," + var_str + "," + changed_var_str + ",";

				str=str[0:len(str)-1]+")."
				str_divider=str_divider[0:len(str_divider)-1]+")."

				Src_Not:=model.Negators.Replace(line.Src)
				if line.Src!="" {str=str[0:len(str)-1]+ " :- " + Src_Not +"."}
				if line.Src!="" {str_divider=str_divider[0:len(str_divider)-1]+ " :- " + model.Splitter_A.ReplaceAllString(Src_Not, "${0}_") +"."}
				
				Final_Code.Transitions=append(Final_Code.Transitions,str)
				Final_Code.Transitions=append(Final_Code.Transitions,str_divider)
			}
		}	
	}
		transitions_str=transitions_str[0:len(transitions_str)-1]
		transitions_str_big=transitions_str_big[0:len(transitions_str_big)-1]
		str = "scope_symbols(" + "[" + strings.ToLower(var_str)+ "," + strings.ToLower(var_str_prime) + "]," + " ["+transitions_str+"]" + ")."
		Final_Code.Scope_Symbols=append(Final_Code.Scope_Symbols,str)
		str = "scope_symbols(" + "[" + strings.ToLower(var_str_)+ "," + strings.ToLower(var_str)+ "," + strings.ToLower(var_str_prime) + "]," + " ["+transitions_str_big+"]" + ")."
		Final_Code.Scope_Symbols=append(Final_Code.Scope_Symbols,str)

}