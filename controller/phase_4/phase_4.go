package phase_4

import (
	"../../model"
	"bufio"
    "fmt"
    "log"
    "os"
    "strings"
    "strconv"
    "regexp"
    "os/exec"
)

type pair struct{
	a string
	b string
}

var constant = regexp.MustCompile("^[0-9]+$")
var variable = regexp.MustCompile("[a-zA-Z_][a-zA-Z_0-9]*")
var eq = regexp.MustCompile("[a-zA-Z_0-9]*=[a-zA-Z_0-9+]*")
var less = regexp.MustCompile("[a-zA-Z_0-9+]+<[a-zA-Z_0-9+]+")
var lesseq = regexp.MustCompile("[a-zA-Z_0-9+]+=<[a-zA-Z_0-9+]+")
var greater = regexp.MustCompile("[a-zA-Z_0-9+]+>[a-zA-Z_0-9+]+")
var greatereq = regexp.MustCompile("[a-zA-Z_0-9+]+>=[a-zA-Z_0-9+]+")
var numplus = regexp.MustCompile("([0-9]+)\\+(.*)")


func space(a string,i int)int{
	for ;i<len(a)&&a[i]==' ';i++{}
	return i
}

var Should_Simplify = true

func sub_clean(inequalities []pair) string {
	//normal simplification
	output:=""
	null_pair:=pair{"0","0"}
	changed:=false
	for ; ; {
		for i := 0; i < len(inequalities); i++ {
			if inequalities[i]==null_pair {
				continue
			}
			for j := i+1; j < len(inequalities); j++ {
				if inequalities[j]==null_pair {
					continue
				}
				if inequalities[i].a==inequalities[j].a {
					if inequalities[i].b==inequalities[j].b {
						inequalities[i]=null_pair
						changed=true
						break
					}else if constant.MatchString(inequalities[i].b) && constant.MatchString(inequalities[j].b){
						b1,_:=strconv.Atoi(inequalities[i].b)
						b2,_:=strconv.Atoi(inequalities[j].b)
						if b2>b1 {
							b2=b1
						}
						inequalities[j].b=strconv.Itoa(b2)
						inequalities[i]=null_pair
						changed=true
						break
					}else if match,_:=regexp.MatchString("=" + inequalities[i].b + "[+][0-9]+","=" +inequalities[j].b);match {
						inequalities[i]=null_pair
						changed=true
						break
					}else if match,_:=regexp.MatchString("=" + inequalities[j].b + "[-][0-9]+","=" +inequalities[i].b);match {
						inequalities[i]=null_pair
						changed=true
						break
					}
				}else if inequalities[i].b==inequalities[j].b {
					if constant.MatchString(inequalities[i].a) && constant.MatchString(inequalities[j].a){
						a1,_:=strconv.Atoi(inequalities[i].a)
						a2,_:=strconv.Atoi(inequalities[j].a)
						if a2<a1 {
							a2=a1
						}
						inequalities[j].a=strconv.Itoa(a2)
						inequalities[i]=null_pair
						changed=true
						break
					}else if match,_:=regexp.MatchString("=" + inequalities[i].a + "[+][0-9]+","=" +inequalities[j].a);match {
						inequalities[i]=null_pair
						changed=true
						break
					}else if match,_:=regexp.MatchString("=" + inequalities[j].a + "[-][0-9]+","=" +inequalities[i].a);match {
						inequalities[i]=null_pair
						changed=true
						break
					}
				}else if inequalities[i].b==inequalities[j].a&&inequalities[i].a==inequalities[j].b {
					inequalities[i].b=numplus.ReplaceAllString(inequalities[i].b,"( + $1 $2 )")
					inequalities[i].a=numplus.ReplaceAllString(inequalities[i].a,"( + $1 $2 )")
					new_eq:=" ( = " + inequalities[i].a+ " " +inequalities[i].b +" )"
					
					if !strings.Contains(output, new_eq){
						output=output+ new_eq
					}
					inequalities[i]=null_pair
					inequalities[j]=null_pair
					changed=true
					break
				}
			}	
		}
		if !changed {
			break
		}
		changed=false
	}
	for i := 0; i < len(inequalities); i++ {
		if inequalities[i].a!="0" || inequalities[i].b!="0" {
			inequalities[i].b=numplus.ReplaceAllString(inequalities[i].b,"( + $1 $2 )")
			inequalities[i].a=numplus.ReplaceAllString(inequalities[i].a,"( + $1 $2 )")
			output=output+"( <= "+inequalities[i].a+" "+inequalities[i].b+" ) "
		}
	}
	return "( and " + output + " ) "
}

func map_ineq(inequalities *[]pair, s string) {
	splits:=[]string{}
	if lesseq.MatchString(s){
		splits=strings.Split(s,"=<")
		(*inequalities)=append((*inequalities),pair{splits[0],splits[1]})
	}else if greatereq.MatchString(s){
		splits=strings.Split(s,">=")
		(*inequalities)=append((*inequalities),pair{splits[1],splits[0]})
	}else if eq.MatchString(s) {
		splits=strings.Split(s,"=")
		(*inequalities)=append((*inequalities),pair{splits[0],splits[1]})
		(*inequalities)=append((*inequalities),pair{splits[1],splits[0]})
	}/*else if less.MatchString(s){
		splits=strings.Split(s,"<")
		(*inequalities)=append((*inequalities),pair{splits[0],splits[1]+"-1"})
	}else if greater.MatchString(s){
		splits=strings.Split(s,">")
		(*inequalities)=append((*inequalities),pair{splits[1],splits[0]+"-1"})
	}*/
}

func clean(s string,declarations string) string {
	//fmt.Println(s)
	if !Should_Simplify {
		return s
	}
	inequalities := []pair{}	// a <= b
	output := ""
	str:=""

	for i := 0; i < len(s); i++ {
		if s[i:i+1]==","||s[i:i+1]=="("||s[i:i+1]==")"||s[i:i+1]==";" {
			map_ineq(&inequalities,str)
			str=""
		}else if i==len(s)-1 {
			str=str+s[i:i+1]
			map_ineq(&inequalities,str)
			str=""
		}else{
			str=str+s[i:i+1]
		}
		if s[i:i+1]=="("||s[i:i+1]==")"||s[i:i+1]==";" {
			if s[i:i+1]=="(" {
				output=output+sub_clean(inequalities)+ " " + s[i:i+1] + " or "
			}else if s[i:i+1]==";" {
				output=output+sub_clean(inequalities) + " "
			}else{
				output=output+sub_clean(inequalities)+ " "+s[i:i+1]
			}
			inequalities = []pair{}
		}else if i==len(s)-1 {
			output=output+sub_clean(inequalities)
			inequalities = []pair{}
		}
	}
	output="( and " + output + " ) "
	//return output

	// z3 simplification loop. Possible place if you are in infinite loop while simplifying

	tmp:=""

	for ; ; {
		dumpfile, err := os.Create("/tmp/simplify")
	    if err!=nil {
	    	panic(err)
	    	
	    }

	    dumpfile.Write([]byte(declarations+ "( assert " +output + ")  (apply (then simplify ctx-simplify ctx-solver-simplify)) \n" ))

		dump,err2 := exec.Command("z3", "-smt2" ,"/tmp/simplify").Output()
		if err2!=nil  {
			fmt.Println("This could not used as input by z3 - ",declarations+ "( assert " +output + ")  (apply (then simplify ctx-simplify ctx-solver-simplify)) " )
			if strings.Contains(err2.Error(),"exit status 1"){
				break
			}
			panic(err2)	
		}
		tmp=string(dump)[13:len(dump)-31]
		tmp=regexp.MustCompile("[\n]+").ReplaceAllString(tmp," ")
		tmp="( and " + tmp + " ) "
		if output==tmp {
			break
		}
		output=tmp
	}
	_ = exec.Command("rm" ,  "tmp/simplify" ).Run()

	output=strings.Replace(output,"(","( ",-1)
	output=strings.Replace(output,")"," )",-1)
	return output
}

//stack based implementation to convert smt2 format to easily readable format. check any .out files from commit on 19th july,2016
func backtocommas(str string) string {
	if !Should_Simplify {
		return str
	}
	//return str
	str=regexp.MustCompile("[(] [-] ([0-9]+) [)]").ReplaceAllString(str,"-$1")
	str=strings.Replace(str,"( - 1 )","-1",-1)
	mains := new(model.Stack)
	operator := new(model.Stack)
	tmp:=new(model.Stack)
	dollar := new(string)
	*dollar="$"

	op:=false
	str_array:=regexp.MustCompile("[ ]+").Split(str,-1)
	for _, r := range str_array {
        c :=  new(string)
        (*c)=r
        if op {
        	if (*c)=="and" {
        		*c=","
        	}
        	if (*c)=="or" {
        		*c=" ; "
        	}
        	operator.Push(c)
        	op=false
        }else if *c=="(" {
        	mains.Push(c)
        	op=true
        }else if *c==")"  {
 			for c=mains.Pop().(*string) ; *c!="("; c=mains.Pop().(*string)  {
 					tmp.Push(c)
 			}   
 			if tmp.Len()==0 {
 				tmp.Push(dollar)
 			}
 			s:=*(tmp.Pop().(*string))    	
 			opr:=operator.Pop().(*string)
 			if *opr==" ; " {
 				s="( "+s
 			}
 			for tmp.Len() > 0 {
 				s=s+*opr+*(tmp.Pop().(*string))
 			}
 			if *opr==" ; " {
 				s=s+" )"
 			}
 			*c=s
 			mains.Push(c)
        }else{
        	mains.Push(c)
        }
        
    }
    var s string
	for mains.Len() > 0 {
		s=s+*(mains.Pop().(*string))
	}
    return s

}

func Attach_Results(Base_File string, Dump_File string, Out_File string,all_symbols []string,ignore_for_inv *map[int]bool,mayisimplify bool) bool {
	Should_Simplify=mayisimplify
	var invariants map[string]string	
	invariants = make(map[string]string)

	Program_Correct:=false
	looking:=true

	declarations:=""
	for _,variable := range all_symbols{
		declarations = declarations + "(declare-const " + strings.ToLower(variable) + " Int) "
	}

	file, err := os.Open(Dump_File)
    if err != nil {
        log.Fatal(err)
    }

    r := bufio.NewReader(file)
	s, e := model.Readln(r)
	for e == nil {
		if strings.Contains(s,"program is correct") {
			Program_Correct=true
		}
		
		if len(s)>=3 && s[0:3] == "inv" && looking{
			splits := strings.SplitN(s, " ",2)
			if len(splits) > 1 { invariants[strings.Split(splits[0],"-")[0]]=splits[1] }
		}
		if strings.Contains(s,"Time") {
			fmt.Println(s)
		}
		s,e = model.Readln(r)
	}

    file.Close()


    basefile, err2 := os.Open(Base_File)
    if err2 != nil {
        log.Fatal(err2)
    }
    outfile, err3 := os.Create(Out_File)
    if err3 != nil {
        log.Fatal(err3)
    }

    r = bufio.NewReader(basefile)
    
    thread := 0
    line := 0
    way := 0
    pre := false

	ignore_count:=0
    s, e = model.Readln(r)
	for e == nil {
		if len(s) > 1 && s[0:1]=="{" && pre==false {
			outfile.WriteString(s + "\n"+"----------\n")
			s,e = model.Readln(r)
			ignore_count=ignore_count+1
			pre=true
			continue
		}
		if s=="||" || (len(s) > 1 && s[0:1]=="{") {
			way=0
			result := invariants["inv" + strconv.Itoa(thread) + "_" + strconv.Itoa(line) + "_" + strconv.Itoa(way)]
			outfile.WriteString("{Final_" + strconv.Itoa(thread) + " : " + backtocommas(clean(result[space(result,0):],declarations)) + "}\n")
			line=0
			thread++
			outfile.WriteString(s + "\n"+"----------\n")
			s,e = model.Readln(r)
			ignore_count=ignore_count+1
			continue
		}
		if _, ok := (*ignore_for_inv)[ignore_count]; ok {
			outfile.WriteString(s + "\n")
			s,e = model.Readln(r)
			ignore_count=ignore_count+1
	    	continue
	    }
	    if len(s)>=5&&s[0:5]=="hint-"{
			way=0
			s,e = model.Readln(r)
			ignore_count=ignore_count+1
			continue
	    }

		for ;;way++ {
			result, ok := invariants["inv" + strconv.Itoa(thread) + "_" + strconv.Itoa(line) + "_" + strconv.Itoa(way)]
			if ok {
				outfile.WriteString("{" + strconv.Itoa(way) + " : " + backtocommas(clean(result[space(result,0):],declarations)) + "}\n")
			}else{
				outfile.WriteString(s + "\n"+"----------\n")
				if way>1 {
					s,e = model.Readln(r)
					ignore_count=ignore_count+1
					outfile.WriteString(s + "\n"+"----------\n")
				}
				break
			}
		}
		line++
		way=0
		s,e = model.Readln(r)
		ignore_count=ignore_count+1
	}

	/*if Program_Correct {
		outfile.WriteString("\n**********************Proved**********************\n")
	}else{
		outfile.WriteString("\n****************Could Not Prove*********************\n")
	}*/


    basefile.Close()
    outfile.Close()
    return Program_Correct
}
