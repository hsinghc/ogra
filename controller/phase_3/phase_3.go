package phase_3

import (
	"../../model"
	"strconv"
	"strings"
)

func Produce_Invariants(symbols []string,all_symbols []string,code *model.Base_Code,Final_Code *model.Final_Code) {
	
	var str string
	var global_var_str string
	var global_var_str1 string
	var global_var_str_ string
	var all_var_str string
	var all_var_str1 string
	var all_var_str_ string
	var inv_str string


	for _,variable := range symbols{
				global_var_str=global_var_str+variable+","
				global_var_str1=global_var_str1+variable+"1,"
				global_var_str_=global_var_str_+variable+"_,"
			}
	for _,variable := range all_symbols{
				all_var_str=all_var_str+variable+","
				all_var_str1=all_var_str1+variable+"1,"
				all_var_str_=all_var_str_+variable+"_,"
			}
	global_var_str=global_var_str[0:len(global_var_str)-1]
	global_var_str1=global_var_str1[0:len(global_var_str1)-1]
	global_var_str_=global_var_str_[0:len(global_var_str_)-1]
	all_var_str=all_var_str[0:len(all_var_str)-1]
	all_var_str1=all_var_str1[0:len(all_var_str1)-1]
	all_var_str_=all_var_str_[0:len(all_var_str_)-1]

	for indt, thread := range code.Threads{
		inv_str=""
		locals:=thread.Local_Variables_Str
		locals_str:=strings.Join(locals,",")
		var_str:=global_var_str
		var_str_:=global_var_str_
		var_str1:=global_var_str1
		if locals_str!="" {
			var_str=global_var_str+","+locals_str
			var_str_=global_var_str_+","+model.Splitter_A.ReplaceAllString(locals_str, "${0}_")
			var_str1=global_var_str1+","+model.Splitter_A.ReplaceAllString(locals_str, "${0}1")
		}
		str="inv"+strconv.Itoa(indt)+"_0_0(" + var_str + ") :- init(" + all_var_str + ")."
		Final_Code.Locals=append(Final_Code.Locals,str)

		str="inv_C"+strconv.Itoa(indt)+"_0(" + var_str + ") :- inv"+strconv.Itoa(indt)+"_0_0(" + var_str + ")."
		Final_Code.Summary=append(Final_Code.Summary,str)

		/*str=" inv"+strconv.Itoa(indt)+"_0_0(" + var_str + ")" + " :- " + "inv_C"+strconv.Itoa(indt)+"_0(" + var_str + ")" + "."
		Final_Code.Summary=append(Final_Code.Summary,str)*/
		
		inv_str=inv_str+"inv"+strconv.Itoa(indt)+"_0_0,"
		inv_str=inv_str+"inv_C"+strconv.Itoa(indt)+"_0,"

		for indl,_ := range thread.Lines{
			for i := 0; i < len(thread.Lines[indl].Extra_Pre); i++ {
				str="inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl)+"_"+strconv.Itoa(i+1)+"(" + var_str + ") :- " +"inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl)+"_0(" + var_str + "),"+ thread.Lines[indl].Extra_Pre[i]+"."
				Final_Code.Locals=append(Final_Code.Locals,str)
				inv_str=inv_str+"inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl)+"_"+strconv.Itoa(i+1) +","
			}

			if model.Splitter_C.MatchString(thread.Lines[indl].Dest) {
				i, _ := strconv.Atoi(thread.Lines[indl].Dest)
				str="inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(i-1)+"_0(" + var_str1 + ") :- " +"inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl)+"_0(" + var_str + "),"+"next_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(i-1)+ "(" + all_var_str+","+all_var_str1+")." 
				Final_Code.Locals=append(Final_Code.Locals,str)
				
				str="inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(i-1)+"(" + var_str_ + ") :- inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(i-1)+"_0(" + var_str_ + ") ; inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl)+"(" + var_str_ + ")."
				Final_Code.Summary=append(Final_Code.Summary,str)	

				/*str=" \\+(\\+(inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(i-1)+"_0(" + var_str_ + ")) , \\+(inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl)+"(" + var_str_ + ")))" + ":-" + "inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(i-1)+"(" + var_str_ + ") " + "."
				Final_Code.Summary=append(Final_Code.Summary,str)*/		

			if thread.Lines[indl].Src!="" {
					str="inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1)+"_0(" + var_str1 + ") :- " +"inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl)+"_0(" + var_str + "),"+"next_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(indl+1)+ "(" + all_var_str+","+all_var_str1+")." 
					Final_Code.Locals=append(Final_Code.Locals,str)
				
					str="inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1)+"(" + var_str_ + ") :- inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1)+"_0(" + var_str_ + ") ; inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl)+"(" + var_str_ + ")."
					Final_Code.Summary=append(Final_Code.Summary,str)

					/*str="\\+(\\+(inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1)+"_0(" + var_str_ + ")) , \\+(inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl)+"(" + var_str_ + ")))"+":-"+ "inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1)+"(" + var_str_ + ")" + "."
					Final_Code.Summary=append(Final_Code.Summary,str)*/

					inv_str=inv_str+"inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1) + "_0,"
					inv_str=inv_str+"inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1) + ","
				}else{
					inv_str=inv_str+"inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1) + "_0,"
					inv_str=inv_str+"inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1) + ","
				}

			}else{
				str="inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1)+"_0(" + var_str1 + ") :- " +"inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl)+"_0(" + var_str + "),"+"next_"+strconv.Itoa(indt)+"_" + strconv.Itoa(indl) + "_" + strconv.Itoa(indl+1)+ "(" + all_var_str+","+all_var_str1+")." 
				Final_Code.Locals=append(Final_Code.Locals,str)
				
				str="inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1)+"(" + var_str_ + ") :- inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1)+"_0(" + var_str_ + ") ; inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl)+"(" + var_str_ + ")."
				Final_Code.Summary=append(Final_Code.Summary,str)

				/*str="\\+(\\+(inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1)+"_0(" + var_str_ + ")) , \\+(inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl)+"(" + var_str_ + ")))"+":-"+"inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1)+"(" + var_str_ + ")"+"."
				Final_Code.Summary=append(Final_Code.Summary,str)*/

				inv_str=inv_str+"inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1) + "_0,"
				inv_str=inv_str+"inv_C"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1) + ","
			}
			for _,hint := range thread.Lines[indl].Hint{
				if hint!="" {
					str= hint + " :- inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(indl+1)+"_0(" + var_str + ") ."
					Final_Code.Summary=append(Final_Code.Summary,str)
				}
			}	
		}

		Final_Code.Post_Condition[0]=Final_Code.Post_Condition[0] + "inv"+strconv.Itoa(indt)+"_"+strconv.Itoa(len(thread.Lines))+"_0(" + var_str + ")" + ","
		inv_str=inv_str[0:len(inv_str)-1]

		str = "scope_symbols(" + "[" + strings.ToLower(var_str) + "]," + " ["+inv_str+"]" + ")."
		Final_Code.Scope_Symbols=append(Final_Code.Scope_Symbols,str)
		
	}
	Final_Code.Post_Condition[0]=(Final_Code.Post_Condition[0])[0:len(Final_Code.Post_Condition[0])-1]+"."

	for indt1, thread1 := range code.Threads{
		for indt2, thread2 := range code.Threads{
			if indt1==indt2 {
				continue;
			}
			for indl1 := 0; indl1 <= len(thread1.Lines); indl1++ {
				for indl2 := 0; indl2 < len(thread2.Lines); indl2++ {
					t1_locals:=thread1.Local_Variables_Str
					t1_locals_str:=strings.Join(t1_locals,",")
					t1_var_str:=global_var_str
					t1_var_str_:=global_var_str_
					t1_var_str1:=global_var_str1
					if t1_locals_str!="" {
						t1_var_str=global_var_str+","+t1_locals_str
						t1_var_str_=global_var_str_+","+model.Splitter_A.ReplaceAllString(t1_locals_str, "${0}_")
						t1_var_str1=global_var_str1+","+model.Splitter_A.ReplaceAllString(t1_locals_str, "${0}1")
					}
					t2_locals:=thread2.Local_Variables_Str
					t2_locals_str:=strings.Join(t2_locals,",")
					t2_var_str:=global_var_str
					t2_var_str_:=global_var_str_
					if t2_locals_str!="" {
						t2_var_str=global_var_str+","+t2_locals_str
						t2_var_str_=global_var_str_+","+model.Splitter_A.ReplaceAllString(t2_locals_str, "${0}_")
					}
					var length int
					if length = 0;indl1 < len(thread1.Lines){ length = len(thread1.Lines[indl1].Extra_Pre)	}
					for j := 0; j <=length ; j++ {
						var k int
						if k = 0;len(thread2.Lines[indl2].Extra_Pre)>0{ k = 1	}	
						for ; k <= len(thread2.Lines[indl2].Extra_Pre); k++ {
							if  model.Splitter_C.MatchString(thread2.Lines[indl2].Dest) {
								i, _ := strconv.Atoi(thread2.Lines[indl2].Dest)
								if thread2.Lines[indl2].Src!="" {
										str="inv"+strconv.Itoa(indt1)+"_"+strconv.Itoa(indl1)+"_"+strconv.Itoa(j)+"(" + t1_var_str1 + ") :- " + "inv"+strconv.Itoa(indt1)+"_"+strconv.Itoa(indl1)+"_"+strconv.Itoa(j)+"(" + t1_var_str + "),"+  "inv"+strconv.Itoa(indt2)+"_"+strconv.Itoa(indl2)+"_"+strconv.Itoa(k)+"(" + t2_var_str + ")" + "," + "inv_C"+strconv.Itoa(indt1)+"_"+strconv.Itoa(indl1)+"(" + t1_var_str_ + "),"+  "inv"+strconv.Itoa(indt2)+"_"+strconv.Itoa(indl2)+"_"+strconv.Itoa(k)+"(" + t2_var_str_ + ")" + ","  + "dnext_"+strconv.Itoa(indt2)+"_" + strconv.Itoa(indl2) + "_" + strconv.Itoa(indl2+1)+ "(" + all_var_str_+"," + all_var_str + ","+all_var_str1+")." 
										Final_Code.Stability=append(Final_Code.Stability,str)
									}
								str="inv"+strconv.Itoa(indt1)+"_"+strconv.Itoa(indl1)+"_"+strconv.Itoa(j)+"(" + t1_var_str1 + ") :- " + "inv"+strconv.Itoa(indt1)+"_"+strconv.Itoa(indl1)+"_"+strconv.Itoa(j)+"(" + t1_var_str + "),"+  "inv"+strconv.Itoa(indt2)+"_"+strconv.Itoa(indl2)+"_"+strconv.Itoa(k)+"(" + t2_var_str + ")" + "," + "inv_C"+strconv.Itoa(indt1)+"_"+strconv.Itoa(indl1)+"(" + t1_var_str_ + "),"+  "inv"+strconv.Itoa(indt2)+"_"+strconv.Itoa(indl2)+"_"+strconv.Itoa(k)+"(" + t2_var_str_ + ")" + ","  + "dnext_"+strconv.Itoa(indt2)+"_" + strconv.Itoa(indl2) + "_" + strconv.Itoa(i-1)+ "(" + all_var_str_+"," + all_var_str + ","+all_var_str1+")." 
								Final_Code.Stability=append(Final_Code.Stability,str)
							}else{
								if thread2.Lines[indl2].Flush {
									str="inv"+strconv.Itoa(indt1)+"_"+strconv.Itoa(indl1)+"_"+strconv.Itoa(j)+"(" + t1_var_str1 + ") :- " + "inv"+strconv.Itoa(indt1)+"_"+strconv.Itoa(indl1)+"_"+strconv.Itoa(j)+"(" + t1_var_str + "),"+  "inv"+strconv.Itoa(indt2)+"_"+strconv.Itoa(indl2)+"_"+strconv.Itoa(k)+"(" + t2_var_str + ")" + "," +  "next_"+strconv.Itoa(indt2)+"_" + strconv.Itoa(indl2) + "_" + strconv.Itoa(indl2+1)+ "(" + all_var_str + ","+all_var_str1+")." 
									Final_Code.Stability=append(Final_Code.Stability,str)
								}else{
									str="inv"+strconv.Itoa(indt1)+"_"+strconv.Itoa(indl1)+"_"+strconv.Itoa(j)+"(" + t1_var_str1 + ") :- " + "inv"+strconv.Itoa(indt1)+"_"+strconv.Itoa(indl1)+"_"+strconv.Itoa(j)+"(" + t1_var_str + "),"+  "inv"+strconv.Itoa(indt2)+"_"+strconv.Itoa(indl2)+"_"+strconv.Itoa(k)+"(" + t2_var_str + ")" + "," + "inv_C"+strconv.Itoa(indt1)+"_"+strconv.Itoa(indl1)+"(" + t1_var_str_ + "),"+  "inv"+strconv.Itoa(indt2)+"_"+strconv.Itoa(indl2)+"_"+strconv.Itoa(k)+"(" + t2_var_str_ + ")" + ","  + "dnext_"+strconv.Itoa(indt2)+"_" + strconv.Itoa(indl2) + "_" + strconv.Itoa(indl2+1)+ "(" + all_var_str_+"," + all_var_str + ","+all_var_str1+")." 
									Final_Code.Stability=append(Final_Code.Stability,str)
								}
							}
						}
					}
				}
			}	
		}
	}


	
}


/* by initial declaration with next we mean that that corresponding tuple should stay
and rest we wish to satisfy - 


1. summary invariants to be introduced -- done
2. local conditions set -- done
2. stability conditions set -- done 
3. issue with +,multiple variables --done
4. check for weak updates
5. Atomic Ones -- Done for format ++,--
6. If,while
*/ 