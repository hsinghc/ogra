package main

import (
	"./phase_0"
	"./phase_1"
	"./phase_2"
	"./phase_3"
	"./phase_4"
	"../model"
	"strings"
	"os"
	"os/exec"
	"fmt"
)



func main() {

	var code model.Base_Code
	var global_symbols []string
	var all_symbols []string

	var final_code model.Final_Code
	var ignore_for_inv map[int]bool
    ignore_for_inv=make(map[int]bool)


	phase_0.Label_the_loops(os.Args[1],"/tmp/label_the_loops",&ignore_for_inv);
	phase_1.Parse_To_Base_Code("/tmp/label_the_loops",&code,&global_symbols,&all_symbols);
	phase_2.Parse_To_Transition_Code(global_symbols,all_symbols,&code,&final_code)
	phase_3.Produce_Invariants(global_symbols,all_symbols,&code,&final_code)

	outfile, err := os.Create(os.Args[1] + ".in")
    if err!=nil {
    	panic(err)
    	
    }


/*	model.Print_array(final_code.Init,outfile)
	model.Print_array(final_code.Transitions,outfile)
	model.Print_array(final_code.Locals,outfile)
	model.Print_array(final_code.Summary,outfile)
	model.Print_array(final_code.Stability,outfile)
	model.Print_array(final_code.Post_Condition,outfile)
	model.Print_array(final_code.Scope_Symbols,outfile)*/

	tool_exist:=true

	if len(os.Args)<=2 || os.Args[2]=="--ns" {
		tool_exist=false;
	}

	model.Print_array([]string{"/*Variable List*/"},outfile)
    model.Print_array([]string{strings.Join(append(append([]string{"/*"},all_symbols...),"*/")," ")},outfile)
    model.Print_array([]string{"/*Init*/"},outfile)
    model.Print_array([]string{"/*" + strings.Split(final_code.Init[0],":-")[0] + ".*/"},outfile)
    model.Print_array([]string{"/*Transitions*/"},outfile)
	model.Print_array(final_code.Transitions,outfile)
    model.Print_array([]string{"/*Rules*/"},outfile)
	model.Print_array(final_code.Init,outfile)
	model.Print_array(final_code.Locals,outfile)
	model.Print_array(final_code.Summary,outfile)
	model.Print_array(final_code.Stability,outfile)
	model.Print_array(final_code.Post_Condition,outfile)
    model.Print_array([]string{"/*Scope Symbols*/"},outfile)
	model.Print_array(final_code.Scope_Symbols,outfile)

	outfile.Close()

	fmt.Println("Tool Running")

	if tool_exist{
		dump,err2 := exec.Command(os.Args[2] , "-debug" ,os.Args[1] + ".in" ).Output()
			if err2!=nil {
				fmt.Println(err2.Error())	
				/*panic(err2)	*/
			}

			dumpfile, err3 := os.Create(os.Args[1] + ".dump")
		    if err3!=nil {
		    	panic(err3)
		    	
		    }

		    dumpfile.Write(dump)
		    dumpfile.Close()
		    simplify:=true

		    if len(os.Args)>3 && os.Args[3]=="--ns"{
		    	simplify=false
		    }

			phase_4.Attach_Results(os.Args[1],os.Args[1] + ".dump",os.Args[1] + ".out",all_symbols,&ignore_for_inv,simplify)
			fmt.Println("Done. Check " + os.Args[1] + ".out" + " for Results")
		
			dump,err2 = exec.Command("rm" ,  os.Args[1] + ".dump" ).Output()
			if err2!=nil {
				fmt.Println(err2.Error())	
				panic(err2)	
			}
		}



	/*if Proof {
		fmt.Println("Proved")
	}else{
		fmt.Println("Could Not Prove")
	}
*/
	_ = exec.Command("rm" ,  "/tmp/label_the_loops" ).Run()

}


/*	Init []string
	Transitions []string
	Locals []string
	Summary []string
	Stability []string
	Post_Condition string
	Scope_Symbols []string
*/