package phase_0


import (
	"bufio"
    "os"
    "regexp"
    "strings"
    "strconv"
    "log"
    "../../model"
)





func Label_the_loops(filename string,dumpfilename string,ignore_for_inv *map[int]bool) {


	code := []string{}
	begins := new(model.Stack)
	// counts to generate distinct labels
	ifcount := 0
	whilecount := 0
	wastecount := 0

	file, err := os.Open(filename)
    if err != nil {
        log.Fatal(err)
    }

    ignore_count:=0

    r := bufio.NewReader(file)
	s, e := model.Readln(r)
	for e == nil {
		// removing white space
		s=regexp.MustCompile("[ \t]+").ReplaceAllString(s,"")
		if strings.Contains(s,"ite(") {
			// ite is implemented as ifgoto to just next line
			splits:=strings.Split(s,"ite(")
			splits[1]=splits[1][0:len(splits[1])-1]
			splits=append(strings.Split(splits[1],","),splits[0])
			code=append(code,"ifgoto,waste_label"+strconv.Itoa(wastecount)+","+splits[0]+"|"+splits[3][0:len(splits[3])-1]+"|"+splits[1]+"|"+splits[2]+"\nwaste_label"+strconv.Itoa(wastecount)+":")
			wastecount=wastecount+1
		}else if (len(s)>2 && s[0:2]=="if") || (len(s)>5&&s[0:5]=="while") {
			code=append(code,s)
			begins.Push(len(code)-1)
		}else if s=="}" {
			ind := begins.Pop().(int)
			s = code[ind]
			if s=="}else{" {
				(*ignore_for_inv)[ignore_count]=true
				ind_if := begins.Pop().(int)
				number:=strconv.Itoa(ifcount)
				ifcount=ifcount+1
				cond := strings.SplitN(code[ind_if],"(",2)[1]
				cond = cond[0:len(cond)-2]
				cond = strings.Replace(cond,"&&",",",-1)
				cond = strings.Replace(cond,"||",";",-1)
				not_cond := model.Negators.Replace(cond)
				code[ind_if] = "ifgoto,else"+number+","+not_cond
				code[ind] = "ifgoto,ifend"+number+",\nelse"+number+":"
				code=append(code,"ifend"+number+":")
			}else if s[0:2]=="if" {
				(*ignore_for_inv)[ignore_count]=true
				cond := strings.SplitN(s,"(",2)[1]
				cond = cond[0:len(cond)-2]
				cond = strings.Replace(cond,"&&",",",-1)
				cond = strings.Replace(cond,"||",";",-1)
				not_cond := model.Negators.Replace(cond)
				number:=strconv.Itoa(ifcount)
				ifcount=ifcount+1
				code[ind] = "ifgoto,ifend"+number+","+not_cond
				code=append(code,"ifend"+number+":")
			}else if s[0:5]=="while"{
				cond := strings.SplitN(s,"(",2)[1]
				cond = cond[0:len(cond)-2]
				cond = strings.Replace(cond,"&&",",",-1)
				cond = strings.Replace(cond,"||",";",-1)
				not_cond := model.Negators.Replace(cond)
				number:=strconv.Itoa(whilecount)
				code[ind] = "while_begin" + number + ":\n" + "ifgoto,while_end"+ number+","+not_cond
				code=append(code,"ifgoto,while_begin"+number+",\nwhile_end"+number+":")
				whilecount=whilecount+1
			}			
		}else if s=="}else{" {
			code=append(code,s)
			begins.Push(len(code)-1)
		}else{
			code=append(code,s)
			if s=="||" {
				ifcount=0
				whilecount=0
			}
		}
		s,e = model.Readln(r)
		ignore_count=ignore_count+1
	}

    file.Close()

    dumpfile, err := os.Create(dumpfilename)
	    if err!=nil {
	    	panic(err)
	    	
	    }
	model.Print_array(code,dumpfile)

    dumpfile.Close()
}