package phase_1

import (
	"../../model"
	"bufio"
    //"fmt"
    "log"
    "os"
    "strings"
    "strconv"
    "sort"
)



func Parse_To_Base_Code(filename string, code *model.Base_Code, symbols *[]string,all_symbols *[]string) {
	file, err := os.Open(filename)
    if err != nil {
        log.Fatal(err)
    }

    var symbols_map map[string]bool
    symbols_map=make(map[string]bool)

    var labels_map map[string]int
    labels_map=make(map[string]int)

    var tmp_thread model.Thread
    r := bufio.NewReader(file)
	s, e := model.Readln(r)
	for e == nil {
	    if s[0:1] == "{" {
	    	if (code.Pre_Condition=="") {
				s=s[1:len(s)-1]
				code.Pre_Condition=strings.ToUpper(s)
			    s,e = model.Readln(r)
				continue
			}else {
				s=s[1:len(s)-1]
				code.Post_Condition=strings.ToUpper(s)
				for j,_ := range tmp_thread.Lines{
					i, ok := labels_map[tmp_thread.Lines[j].Dest]
					if ok{
						tmp_thread.Lines[j].Dest=strconv.Itoa(i)
					}
				}
				labels_map=make(map[string]int)
				code.Threads=append(code.Threads,tmp_thread)
				tmp_thread.Lines=nil
				break
			}
	    }
	    label := ""
	    label_split := strings.Split(strings.ToUpper(s), ":")
	    if len(label_split) ==2 && label_split[1]=="" {
	    	label=label_split[0]
	    }
	    if s=="||" {
			for j,_ := range tmp_thread.Lines{
				i, ok := labels_map[tmp_thread.Lines[j].Dest]
				if ok{
					tmp_thread.Lines[j].Dest=strconv.Itoa(i)
				}
			}
			labels_map=make(map[string]int)
	    	code.Threads=append(code.Threads,tmp_thread)
			tmp_thread.Lines=nil
	    }else if label!=""{
	    	labels_map[label]=len(tmp_thread.Lines)+1
	    }else {
	    	var splits []string
	    	if len(s)>=10 && s[0:10]=="Extra_Pre-" {
	    		tmp_thread.Lines[len(tmp_thread.Lines)-1].Extra_Pre=strings.Split(strings.ToUpper(s[10:]), "|")
	    		splits=[]string{""}
	    	}else if len(s)>=6 && s[0:6]=="ifgoto" {
	    		str_array:=strings.Split(strings.ToUpper(s), "|")
		    	splits = strings.SplitN(str_array[0], ",",3)
	    		tmp_thread.Lines=append(tmp_thread.Lines,model.Line{splits[1],splits[2],false,false,false,[]string{},[]string{},[]string{}}) // dest = line number, SRC = blank
	    		splits = []string{""}
    			if len(str_array)>1{
    				ind:=len(tmp_thread.Lines)-1
    				tmp_thread.Lines[ind].Ite_statement=str_array[1:]
    				tmp_thread.Lines[ind].Ite=true
    				dst:=tmp_thread.Lines[ind].Ite_statement[0]
    				if strings.Contains(dst,"::"){
    					tmp_thread.Lines[ind].Ite_statement[0]=dst[0:len(dst)-1]
    					tmp_thread.Lines[ind].Ite_Flush=true
    				}
    				tmp_thread.Lines[ind].Ite_statement[0]=tmp_thread.Lines[ind].Ite_statement[0][0:len(tmp_thread.Lines[ind].Ite_statement[0])-1]
    				splits=[]string{tmp_thread.Lines[ind].Ite_statement[0],tmp_thread.Lines[ind].Ite_statement[1]+"+"+tmp_thread.Lines[ind].Ite_statement[2]}
    			}
	    	}else if len(strings.Split(s, "::="))>1{
	    		splits = []string{strings.ToUpper(s),""}
		    	splits=append(splits,"")
		    	tmp_thread.Lines=append(tmp_thread.Lines,model.Line{splits[0],splits[1],false,false,false,[]string{},[]string{},[]string{}})
	    	}else if s=="skip"{
	    		s="SKIP_DUMPER=0"
		    	splits = strings.Split(strings.ToUpper(s), ":=")
		    	splits=append(splits,"")
		    	tmp_thread.Lines=append(tmp_thread.Lines,model.Line{splits[0],splits[1],false,false,false,[]string{},[]string{},[]string{}})
	    	}else if len(s)>=5 && s[0:5]=="hint-"{
	    		s=strings.Split(strings.ToUpper(s),"-")[1]
		    	tmp_thread.Lines[len(tmp_thread.Lines)-1].Hint=append(tmp_thread.Lines[len(tmp_thread.Lines)-1].Hint,s)
	    		splits=[]string{""}
	    	}else if len(s)>=5 && s[0:5]=="fence"{
	    		s=strings.ToUpper(s)
	    		code.Pre_Condition=code.Pre_Condition+"," + s[5:]+"F1=0," + s[5:]+"F2=0"
	    		s=s[5:]+"F1," + s[5:]+"F2::=" + strconv.Itoa(len(code.Threads) + 1) + "," +  s[5:]+"F2+1"
		    	splits = []string{strings.ToUpper(s),""}
		    	splits=append(splits,"")
		    	tmp_thread.Lines=append(tmp_thread.Lines,model.Line{splits[0],splits[1],false,false,false,[]string{},[]string{},[]string{}})
	    	}else{
		    	splits = strings.Split(strings.ToUpper(s), ":=")
		    	splits=append(splits,"")
		    	tmp_thread.Lines=append(tmp_thread.Lines,model.Line{splits[0],splits[1],false,false,false,[]string{},[]string{},[]string{}})
			    }
	    	Dest:=(model.Splitter_B.Split(splits[0], -1))[0]
	    	if Dest!= "" {
	    		Dst_Array := (model.Splitter_A.FindAllString(Dest, -1))
		    	for _,Dst := range Dst_Array{
		    		if Dst!="" && Dst!="SKIP_DUMPER" {symbols_map[Dst]=true}
		    	}
	    	}
	    	if len(splits)>1{
		    	Src_Array := (model.Splitter_A.FindAllString(splits[1], -1))
		    	for _,Src := range Src_Array{
		    		if Src!="" {symbols_map[Src]=true}
		    	}
		    }
	    }
	    s,e = model.Readln(r)
	}

	file.Close()
	for variable,_ := range symbols_map{
		if len(variable) > 3 && variable[0:3]=="LVT" {
			i,_:=strconv.Atoi(strings.Split(variable,"_")[1])
			code.Threads[i-1].Local_Variables_Str=append(code.Threads[i-1].Local_Variables_Str,variable)
		}else{
			(*symbols)=append((*symbols),variable)
		}
			(*all_symbols)=append((*all_symbols),variable)
	}
		sort.Strings((*symbols))
		sort.Strings((*all_symbols))

}